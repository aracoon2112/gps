#ifndef __DEMO_MQTT_H_
#define __DEMO_MQTT_H_


#define BROKER_IP  "demo.pyroject.com"
#define BROKER_PORT 1883
#define CLIENT_USER NULL
#define CLIENT_PASS NULL
#define SUBSCRIBE_TOPIC "rf24/gps"
#define PUBLISH_TOPIC   "rf24/gps"
#define PUBLISH_INTERVAL 20000 //10s
#define PUBLISH_PAYLOEAD "hello I'm from gprs module"


#endif