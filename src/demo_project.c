
#include <string.h>
#include <stdio.h>
#include <api_os.h>
#include <api_gps.h>
#include <api_event.h>
#include <api_hal_uart.h>
#include <api_debug.h>
#include "buffer.h"
#include "gps_parse.h"
#include "math.h"
#include "gps.h"
#include "api_hal_pm.h"
#include "time.h"
#include "api_info.h"
#include "assert.h"
#include "api_socket.h"
#include "api_network.h"
#include "api_hal_gpio.h"
#include "api_fs.h"
#include "api_charset.h"
#include "ntp.h"

#define NTP_SERVER "cn.ntp.org.cn"
#define MAIN_TASK_STACK_SIZE    (2048 * 2)
#define MAIN_TASK_PRIORITY      0
#define MAIN_TASK_NAME          "GPS Test Task"
#define TF_CARD_TEST_FILE_NAME "/t/test_TF_card.txt"

static HANDLE mainTaskHandle = NULL;
bool isGpsOn = true;
bool networkFlag = false;
bool system =false;
#define SYSTEM_STATUS_LED GPIO_PIN27
#define UPLOAD_DATA_LED   GPIO_PIN28

typedef struct Pos {
    double lat ;
    double lng ;
} pos ;

char info[100];


bool FsTFTest()
{
    
    int32_t ret;
   // uint8_t *path = TF_CARD_TEST_FILE_NAME;
    int32_t fd;
        uint8_t *path = TF_CARD_TEST_FILE_NAME;
     fd = API_FS_Open(path, FS_O_RDWR | FS_O_CREAT |FS_O_APPEND, 0);

	if ( fd < 0)
	{
        API_FS_Create(TF_CARD_TEST_FILE_NAME,0);
        API_FS_Flush(fd);
      //  Trace(1,"Open file failed:%d",fd);
		return 0;
	}
    //Trace(1,"Start Fs  TF card read write test!");

   
    ret = API_FS_Write(fd,info , strlen(info));
    API_FS_Close(fd);
    while (ret <= 0)
    {
        UART_Write(UART1,"write fail",11);
      //  Trace(1,"write fail");
         ret = API_FS_Write(fd,info , strlen(info));
        OS_Sleep(1000);

    }
    //Trace(1,"write success");
    UART_Write(UART1,"write success",14);
	return true;
}

void EventDispatch(API_Event_t* pEvent)
{
    switch(pEvent->id)
    {
      
        case API_EVENT_ID_GPS_UART_RECEIVED:
            // Trace(1,"received GPS data,length:%d, data:%s,flag:%d",pEvent->param1,pEvent->pParam1,flag);
            GPS_Update(pEvent->pParam1,pEvent->param1);
            break;
     
        case API_EVENT_ID_SYSTEM_READY : 
            system=true;
            break;
        case API_EVENT_ID_UART_RECEIVED:
            if(pEvent->param1 == UART1)
            {
                uint8_t data[pEvent->param2+1];
                data[pEvent->param2] = 0;
                memcpy(data,pEvent->pParam1,pEvent->param2);
              //  Trace(1,"uart received data,length:%d,data:%s",pEvent->param2,data);
                if(strcmp(data,"close") == 0)
                {
                //    Trace(1,"close gps");
                    GPS_Close();
                    isGpsOn = false;
                }
                else if(strcmp(data,"open") == 0)
                {
                  //  Trace(1,"open gps");
                    GPS_Open(NULL);
                    isGpsOn = true;
                }
            }
            break;
             case API_EVENT_ID_NO_SIMCARD:
            //Trace(10,"!!NO SIM CARD%d!!!!",pEvent->param1);
            break;
        
        case API_EVENT_ID_SIMCARD_DROP:
            //Trace(2,"SIM CARD%d DROP",pEvent->param1);
            break;
        
        case API_EVENT_ID_SIGNAL_QUALITY:
            //Trace(2,"signal quality:%d",pEvent->param1);
            break;
        

        case API_EVENT_ID_NETWORK_REGISTERED_HOME:
        case API_EVENT_ID_NETWORK_REGISTERED_ROAMING:
        {
            uint8_t status;
            //Trace(2,"network register success");
            bool ret = Network_GetAttachStatus(&status);
            if(!ret)
                UART_Write(UART1,"get attach staus fail",21);
            //Trace(1,"attach status:%d",status);
            if(status == 0)
            {
                ret = Network_StartAttach();
                if(!ret)
                {
                    UART_Write(UART1,"network attach fail",20);
                }
            }
            else
            {
                Network_PDP_Context_t context = {
                    .apn        ="cmnet",
                    .userName   = ""    ,
                    .userPasswd = ""
                };
                Network_StartActive(context);
            }

            break;
        }
        case API_EVENT_ID_NETWORK_REGISTER_SEARCHING:
            //Trace(2,"network register searching");
            break;
        
        case API_EVENT_ID_NETWORK_REGISTER_DENIED:
           // Trace(2,"network register denied");
            break;

        case API_EVENT_ID_NETWORK_REGISTER_NO:
            //Trace(2,"network register no");
            break;
        
        case API_EVENT_ID_NETWORK_DETACHED:
            //Trace(2,"network detached");
            break;
        
        case API_EVENT_ID_NETWORK_ATTACH_FAILED:
            //Trace(2,"network attach failed");
            break;

        case API_EVENT_ID_NETWORK_ATTACHED:
            UART_Write(UART1,"network attach success",23);
            Network_PDP_Context_t context = {
                .apn        ="cmnet",
                .userName   = ""    ,
                .userPasswd = ""
            };
            Network_StartActive(context);
            break;

        case API_EVENT_ID_NETWORK_DEACTIVED:
            //Trace(2,"network deactived");
            break;
        
        case API_EVENT_ID_NETWORK_ACTIVATE_FAILED:
            //Trace(2,"network activate failed");
            break;
        
        case API_EVENT_ID_NETWORK_ACTIVATED:
            //Trace(2,"network activate success");
            networkFlag = true;
            break;

        // case API_EVENT_ID_NETWORK_GOT_TIME:
        //     Trace(2,"network got time");
        //     break;

        default:
            break;
    }
}
void project_MainTask(void *pData)
{   
    while (!system) OS_Sleep(200);
    time_t timeNTP = 0;
    time_t timeNow;
    
    while(!networkFlag)
        OS_Sleep(200);
     while (timeNTP <= 0)
      {
        if(NTP_Update(NTP_SERVER,5,&timeNTP,true) != 0)
            timeNTP = 0;
        if( timeNTP > 0)
        {
          Trace(1,"ntp get time success,time:%u",timeNTP);
          Trace(1, "timestamp:%d Time: %s",timeNTP, ctime( ( const time_t* ) &timeNTP ));
        }
          OS_Sleep(1000);
      }
     GPS_Info_t* gpsInfo = Gps_GetInfo();
    UART_Write(UART1,"init",5);
        GPS_Init();
    GPS_Open(NULL);
     while(gpsInfo->rmc.latitude.value == 0)
        OS_Sleep(1000);

    for(uint8_t i = 0;i<5;++i)
    {
        bool ret = GPS_SetOutputInterval(10000);
       // Trace(1,"set gps ret:%d",ret);
        if(ret)
            break;
        OS_Sleep(1000);
    }
     if(!GPS_SetLpMode(GPS_LP_MODE_SUPPER_LP))
        //Trace(1,"set gps lp mode fail");

    if(!GPS_SetOutputInterval(1000))
        //Trace(1,"set nmea output interval fail");
    
    UART_Write(UART1,"init ok",8);
      
    while (1)
    {   
 
        if (isGpsOn){
            uint8_t isFixed = gpsInfo->gsa[0].fix_type > gpsInfo->gsa[1].fix_type ?gpsInfo->gsa[0].fix_type:gpsInfo->gsa[1].fix_type;
            char* isFixedStr;            
            if(isFixed == 2)
                isFixedStr = "2D fix";
            else if(isFixed == 3)
            {
                if(gpsInfo->gga.fix_quality == 1)
                    isFixedStr = "3D fix";
                else if(gpsInfo->gga.fix_quality == 2)
                    isFixedStr = "3D/DGPS fix";
            }
            else
                isFixedStr = "no fix";

             int temp = (int)(gpsInfo->rmc.latitude.value/gpsInfo->rmc.latitude.scale/100);
            double latitude = temp+(double)(gpsInfo->rmc.latitude.value - temp*gpsInfo->rmc.latitude.scale*100)/gpsInfo->rmc.latitude.scale/60.0;
            temp = (int)(gpsInfo->rmc.longitude.value/gpsInfo->rmc.longitude.scale/100);
            double longitude = temp+(double)(gpsInfo->rmc.longitude.value - temp*gpsInfo->rmc.longitude.scale*100)/gpsInfo->rmc.longitude.scale/60.0;
            timeNow= time(NULL);
            snprintf(info, sizeof(info),"%f,%f,%ld\n\r",latitude,longitude,timeNow);

            UART_Write(UART1,info, strlen(info));

            FsTFTest();
            
        }
        else Trace(1,"Gps is off");
        OS_Sleep(10000);
    }
    
}

void MainTask(void* param){
    API_Event_t* event =NULL;
   
    UART_Config_t config = {
        .baudRate = UART_BAUD_RATE_115200,
        .dataBits = UART_DATA_BITS_8,
        .stopBits = UART_STOP_BITS_1,
        .parity   = UART_PARITY_NONE,
        .rxCallback = NULL,
        .useEvent   = true
    };

    UART_Init(UART1,config);

    OS_CreateTask(project_MainTask,
        NULL ,NULL, MAIN_TASK_STACK_SIZE,MAIN_TASK_PRIORITY, 0,0,MAIN_TASK_NAME );
    
    while (1){
        if(OS_WaitEvent(mainTaskHandle,(void**)&event,OS_TIME_OUT_WAIT_FOREVER)){
            EventDispatch(event);
            OS_Free(event->pParam1);
            OS_Free(event->pParam2);
            OS_Free(event);
        }
    }
}
void project_Main(void){
    mainTaskHandle = OS_CreateTask(MainTask,
        NULL,NULL,MAIN_TASK_STACK_SIZE,MAIN_TASK_PRIORITY,0,0,MAIN_TASK_NAME);
        OS_SetUserMainHandle(&mainTaskHandle);
}
